$(function () {
    $.fn.imagesLoaded = function (a) {
        function f() {
            a.call(d, b)
        }

        function g() {
            0 >= --c && this.src !== e && (setTimeout(f), b.off("load error", g))
        }
        var b = this.find("img"),
            c = b.length,
            d = this,
            e = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
        return c || f(), b.on("load error", g).each(function () {
            if (this.complete || this.complete === void 0) {
                var a = this.src;
                this.src = e, this.src = a
            }
        }), this
    };
    var a = $("#rg-gallery2"),
        b = a.find("div.es-carousel-wrapper2"),
        c = b.find("ul > li"),
        d = c.length;
    Gallery = function () {
        var e = 0,
            f = "carousel",
            g = !1,
            h = function () {
                c.add('<img src="images/template/docs/photo/ajax-loader.gif"/><img src="images/template/docs/photo/black.png"/>').imagesLoaded(function () {
                    j(), k(), m(c.eq(e))
                }), f === "carousel" && i()
            }, i = function () {
                b.show().elastislide({
                    imageW: 65,
                    onClick: function (a) {
                        return g ? !1 : (g = !0, m(a), e = a.index(), void 0)
                    }
                }), b.elastislide("setCurrent", e)
            }, j = function () {
                var c = $('<a href="#" class="rg-view-full2"></a>'),
                    d = $('<a href="#" class="rg-view-thumbs2 rg-view-selected2"></a>');
                a.prepend($('<div class="rg-view2"/>').append(c).append(d)), c.on("click.rgGallery2", function () {
                    return f === "carousel" && b.elastislide("destroy"), b.hide(), c.addClass("rg-view-selected2"), d.removeClass("rg-view-selected2"), f = "fullview", !1
                }), d.on("click.rgGallery", function () {
                    return i(), d.addClass("rg-view-selected2"), c.removeClass("rg-view-selected2"), f = "carousel", !1
                }), f === "fullview" && c.trigger("click")
            }, k = function () {
                if ($("#img-wrapper-tmpl2").tmpl({
                    itemsCount: d
                }).appendTo(a), d > 1) {
                    var b = a.find("a.rg-image-nav-prev2"),
                        c = a.find("a.rg-image-nav-next2"),
                        e = a.find("div.rg-image2");
                    b.on("click.rgGallery", function () {
                        return l("left"), !1
                    }), c.on("click.rgGallery", function () {
                        return l("right"), !1
                    }), e.touchwipe({
                        wipeLeft: function () {
                            l("right")
                        },
                        wipeRight: function () {
                            l("left")
                        },
                        preventDefaultEvents: !1
                    }), $(document).on("keyup.rgGallery", function (a) {
                        a.keyCode == 39 ? l("right") : a.keyCode == 37 && l("left")
                    })
                }
            }, l = function (a) {
                return g ? !1 : (g = !0, a === "right" ? e + 1 >= d ? e = 0 : ++e : a === "left" && (0 > e - 1 ? e = d - 1 : --e), m(c.eq(e)), void 0)
            }, m = function (d) {
                var h = a.find("div.rg-loading2").show();
                c.removeClass("selected2"), d.addClass("selected2");
                var i = d.find("img"),
                    j = i.data("large"),
                    k = i.data("description");
                $("<img/>").load(function () {
                    a.find("div.rg-image2").empty().append('<img src="' + j + '"/>'), k && a.find("div.rg-caption2").show().children("p").empty().text(k), h.hide(), f === "carousel" && (b.elastislide("reload"), b.elastislide("setCurrent", e)), g = !1
                }).attr("src", j)
            }, n = function (a) {
                b.find("ul").append(a), c = c.add($(a)), d = c.length, b.elastislide("add", a)
            };
        return {
            init: h,
            addItems: n
        }
    }(), Gallery.init()
})

$(function(){$.fn.imagesLoaded=function(a){function f(){a.call(d,b)}function g(){0>=--c&&this.src!==e&&(setTimeout(f),b.off("load error",g))}var b=this.find("img"),c=b.length,d=this,e="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";return c||f(),b.on("load error",g).each(function(){if(this.complete||this.complete===void 0){var a=this.src;this.src=e,this.src=a}}),this};var a=$("#rg-gallery"),b=a.find("div.es-carousel-wrapper"),c=b.find("ul > li"),d=c.length;Gallery=function(){var e=0,f="carousel",g=!1,h=function(){c.add('<img src="images/template/docs/photo/ajax-loader.gif"/><img src="images/template/docs/photo/black.png"/>').imagesLoaded(function(){j(),k(),m(c.eq(e))}),f==="carousel"&&i()},i=function(){b.show().elastislide({imageW:65,onClick:function(a){return g?!1:(g=!0,m(a),e=a.index(),void 0)}}),b.elastislide("setCurrent",e)},j=function(){var c=$('<a href="#" class="rg-view-full"></a>'),d=$('<a href="#" class="rg-view-thumbs rg-view-selected"></a>');a.prepend($('<div class="rg-view"/>').append(c).append(d)),c.on("click.rgGallery",function(){return f==="carousel"&&b.elastislide("destroy"),b.hide(),c.addClass("rg-view-selected"),d.removeClass("rg-view-selected"),f="fullview",!1}),d.on("click.rgGallery",function(){return i(),d.addClass("rg-view-selected"),c.removeClass("rg-view-selected"),f="carousel",!1}),f==="fullview"&&c.trigger("click")},k=function(){if($("#img-wrapper-tmpl").tmpl({itemsCount:d}).appendTo(a),d>1){var b=a.find("a.rg-image-nav-prev"),c=a.find("a.rg-image-nav-next"),e=a.find("div.rg-image");b.on("click.rgGallery",function(){return l("left"),!1}),c.on("click.rgGallery",function(){return l("right"),!1}),e.touchwipe({wipeLeft:function(){l("right")},wipeRight:function(){l("left")},preventDefaultEvents:!1}),$(document).on("keyup.rgGallery",function(a){a.keyCode==39?l("right"):a.keyCode==37&&l("left")})}},l=function(a){return g?!1:(g=!0,a==="right"?e+1>=d?e=0:++e:a==="left"&&(0>e-1?e=d-1:--e),m(c.eq(e)),void 0)},m=function(d){var h=a.find("div.rg-loading").show();c.removeClass("selected"),d.addClass("selected");var i=d.find("img"),j=i.data("large"),k=i.data("description");$("<img/>").load(function(){a.find("div.rg-image").empty().append('<img src="'+j+'"/>'),k&&a.find("div.rg-caption").show().children("p").empty().text(k),h.hide(),f==="carousel"&&(b.elastislide("reload"),b.elastislide("setCurrent",e)),g=!1}).attr("src",j)},n=function(a){b.find("ul").append(a),c=c.add($(a)),d=c.length,b.elastislide("add",a)};return{init:h,addItems:n}}(),Gallery.init()})



