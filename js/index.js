/* --------------------------------------------------------------------

  @author	Dustin Möller <http://minded-design.de/>
			<dustin@minded-design.de>

  Copyright (C) 2013 Dustin Möller. All rights reserved.

  --------------------------------------------------------------------- */
function md_validate(){
		var a = jQuery("#contact-form").validate({
			rules: {
			      name: {
			        minlength: 2,
			        required: true
			      },
			      email: {
			        required: true,
			        email: true
			      },
			      tel: {
			      	number: true
			      },
			      subject: {
			      	minlength: 2,
			        required: true
			      },
			      message: {
			        minlength: 2,
			        required: true
			      }
			    },
			    highlight: function(label) {
			    	$(label).closest('.control-group').addClass('error');
			    },
			    success: function(label) {
			    	label
			    		.closest('.control-group').addClass('success');
			    },
				submitHandler: function (a) {
							var b = {
									name: $("#name").val(),
									email: $("#email").val(),
									subject: $("#subject").val(),
									message: $("#message").val(),
									tel: $("#tel").val(),
									submit: "submit"
	                        };
	                        $.post("/contact.php", b, function () {
	                        	$("#contact-form").hide();
	                        	$("#success").show();
							})
					}
			})
	}
	$(document).ready(function(){
		$('#about-slider').liquidSlider({autoSlideControls: false,});
		$('.hide-me').after('<p class="expand">Mehr Lesen</p>')
		     $('.expand').click(function(){
		               $(this).prev().slideToggle(function() {
		                   $(this).next('.expand').text(function (index, text) {
		                        return (text == 'Mehr Lesen' ? 'Weniger' : 'Mehr Lesen');
		                   });
		               });
		 
		               return false;
		    });

		if(window.innerWidth >= 1600){
			$('#header').css('background','#000000 url(images/template/bgs/top_bg_2000.jpg) 50% 0 no-repeat fixed');
		} else if(window.innerWidth < 1600 && window.innerWidth >= 1250){
			$('#header').css('background','#000000 url(images/template/bgs/top_bg_1500.jpg) 50% 0 no-repeat fixed');
		} else if(window.innerWidth < 1250 && window.innerWidth >= 950){
			$('#header').css('background','#000000 url(images/template/bgs/top_bg_1100.jpg) 50% 0 no-repeat fixed');
		} else if(window.innerWidth < 950){
			$('#header').css('background','#000000 url(images/template/bgs/top_bg_800.jpg) 50% 0 no-repeat fixed');
		}       

		var scroll_pos = 0;			 
		$(document).scroll(function() { 
			scroll_pos = $(this).scrollTop();
			if(scroll_pos > 50) {
				$("#logo_right").fadeOut();
			} else {
				$("#logo_right").fadeIn();
			}
			if(scroll_pos > 350) {
				$(".navbar").css('background', 'rgba(0, 0, 0, 0.8)');
			} else {
				$(".navbar").css('background', 'transparent');
				$(".navbar").css('opacity', '1');
			}
		});
		$('#about_me').click(function(){
			$('.tab1').trigger('click');
		});
		$('#about_preise').click(function(){
			$('.tab2').trigger('click');
		});
		$('#about_review').click(function(){
			$('.tab3').trigger('click');
		});
		md_validate();
		$('#header_arrow').click(function(){
			$('#what_is').slideToggle();
		});

		// $('#submit').click(function(){
		// 	alert('Das Kontaktformular wird überarbeitet.</br> Bitte schreiben Sie mir direkt eine Email an info@jari-fastner.de');
		// });
	 
	});

	$(window).resize(function() {
		if(window.innerWidth >= 1600){
			$('#header').css('background','#000000 url(images/template/bgs/top_bg_2000.jpg) 50% 0 no-repeat fixed');
		} else if(window.innerWidth < 1600 && window.innerWidth >= 1250){
			$('#header').css('background','#000000 url(images/template/bgs/top_bg_1500.jpg) 50% 0 no-repeat fixed');
		} else if(window.innerWidth < 1250 && window.innerWidth >= 950){
			$('#header').css('background','#000000 url(images/template/bgs/top_bg_1100.jpg) 50% 0 no-repeat fixed');
		} else if(window.innerWidth < 950){
			$('#header').css('background','#000000 url(images/template/bgs/top_bg_800.jpg) 50% 0 no-repeat fixed');
		}  
	})


	jQuery.extend(jQuery.validator.messages, {
	    required: "Dies ist ein Pflichtfeld",
	    remote: "Please fix this field.",
	    email: "Geben Sie bitte eine korrekte Email an.",
	    url: "Please enter a valid URL.",
	    date: "Please enter a valid date.",
	    dateISO: "Please enter a valid date (ISO).",
	    number: "Bitte geben Sie eine korrekte Nummer ohne Zeichen an.",
	    digits: "Please enter only digits.",
	    creditcard: "Please enter a valid credit card number.",
	    equalTo: "Please enter the same value again.",
	    accept: "Please enter a value with a valid extension.",
	    maxlength: jQuery.validator.format("Geben Sie bitte maximal {0} Zeichen an."),
	    minlength: jQuery.validator.format("Geben Sie bitte mindestens  {0} Zeichen an."),
	    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
	    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
	    max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
	    min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
	});