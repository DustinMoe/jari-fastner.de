<?php /* --------------------------------------------------------------------

  @author	Dustin Möller <http://minded-design.de/>
			<dustin@minded-design.de>

  Copyright (C) 2013 Dustin Möller. All rights reserved.

  --------------------------------------------------------------------- */ ?>
<section id="header" data-type="background" data-speed="10" class="pages">
	<article class="container">
		<div class="row">
			<div class="span12" id="header_content">
				<img src="images/template/docs/top/slogan.png"/>
				<br/><br/><a href="https://verlieregewichtgewinneleben.de/" target="_blank">Mein Onlineprogramm!</a><br/><br/>
				<span style="font-size:15px; font-family:'Open Sans',italic; letter-spacing:0.7;"><em>Mein Name ist</em></span><br/>
				<span style="font-size:34px; font-family:'Open Sans', semibold; letter-spacing:0.7;">Jari Fastner</span><br/>
				<hr>
				<span style="font-size:15px; font-family:'Open Sans',italic; letter-spacing:0.7;"><em>Ich bin</em></span><br/>
				<span style="font-size:24px; font-family:'Open Sans',semibold; letter-spacing:0.7;">Personal fitness<br/>
				Trainer</span><br/><br/>
				<span style="font-size:18px; font-family:'Open Sans',semibold; letter-spacing:0.7;">Was ist Personaltraining Jari Fastner? <img id="header_arrow" src="images/template/docs/top/arrow.png"/></span>
				<center><div id="what_is"class="hide">Personal Training ist die "Perfektion" der individuellen Trainingsgestaltung.
				Jede Bewegung, jeder Atemzug und jeder Wimpernschlag wird auf das genaueste überwacht und kontrolliert.
				Es gibt kein Trainingsprofil, das motivierender und persönlicher sein könnte.
				Die Trainingseffizienz ist unschlagbar, und die freie Zeiteinteilung, ob Morgens, Abends oder sogar in der Mittagspause, unterstreicht das High-Class Training noch einmal.
				Der Ort und die Trainingsmittel sind frei nach jedem Belieben und Ziel verfügbar.
				Es geht um Sie - tun Sie sich, Ihrer Gesundheit sowie Ihrem Aussehen einen Gefallen und gelangen Sie zu neuer Freude, Energie und Lebensqualität!
				Also lassen Sie es uns gemeinsam angehen, und zusammen erreichen wir endlich mal „Ihre“ lang ersehnten Ziele.﻿﻿</div></center>
			</div>
		</div>
	</article>
</section>