<?php /* --------------------------------------------------------------------

  @author	Dustin Möller <http://minded-design.de/>
			<dustin@minded-design.de>

  Copyright (C) 2013 Dustin Möller. All rights reserved.

  --------------------------------------------------------------------- */ ?>
<section id="about" data-type="background" data-speed="10" class="pages">
	<div class="container">
		<div class="row">
			<div class="span7">
				<div class="liquid-slider" id="about-slider">
					<div>
						<h2 class="title">Über Mich <img src="images/template/docs/about/arrow.png"/></h2>
						<p>
							<img src="images/template/docs/about/about_jari.png"/><br/><br/>
						</p>
						<p>
			          	Mein Name ist <strong>Jari Fastner</strong>, ich bin am 23.06.1987 geboren und staatlich geprüfter Sport- und Gymnastiklehrer.

						Meine qualifizierte, moderne Ausbildung habe ich an der Gluckerschule in Stuttgart gemacht.

						Mit dauerhaft weiteren Fortbildungen halte ich mein Wissen auf dem neuesten Stand, um somit ein absolut zeitgemäßes und effektives Training bieten zu können.<br/><br/>

						Mein Leben lang begleitet mich der Sport schon durch Höhen und Tiefen.

						Daher weiß ich auch genau, wie schwer es ist, seinen "inneren Schweinehund" zu überwinden und Sport zu treiben.

						Darum lassen Sie es uns <strong>gemeinsam</strong> anpacken, und wir ziehen das zusammen durch.

						Denn es ist für mich eine wahre Freude, andere Menschen bei ihren Zielen zu unterstützen und sie anzuleiten.<br/><br/>

						Da Sport meine große <strong>Leidenschaft</strong> ist und - wie schon erwähnt - immer war, kann ich mit absoluter Überzeugung mein Training vermitteln und weiter geben.

						Sport ist nicht nur ein Mittel, um Fett zu verbrennen oder seinen Gesundheitszustand zu verbessern, er ist auch ein nützlicher Gehilfe, um seine <strong>Willenskraft</strong>, <strong>Durchhaltevermögen</strong> oder <strong>Körpergefühl</strong> zu erhöhen.

						Außerdem können Grenzen überwunden und der Teamgeist gefördert werden.

						Sport ist viel mehr, als bloß Gewichte zu heben oder durch die Gegend zu laufen.<br/><br/>

						Selbstständig als Personal Fitnesstrainer bin ich seit 2010.

						Ich liebe meine Arbeit, es gibt nichts Besseres, was ich mir vorstellen könnte.

						Denn schon in meiner Ausbildung stellte sich schnell heraus, was mein späterer beruflicher Werdegang sein sollte: <strong>Personal Fitnesstrainer!</strong><br/><br/>
						
						Ich bin <strong>Personal Trainer in Buchholz</strong> in der Nordheide, Hittfeld, Seevetal, Rosengarten, Tötensen, Hanstedt, Schneverdingen, Nordheide, Tostedt, Hollenstedt, Jesteburg, Bendestorf, Hamburg und Umgebung.<br/><br/>

						Ich bin <strong>Spezialist für Gewichtsreduktion</strong> (Abnehmen) und besuche meine Klienten zu Hause (Hausbesuche).

						Sie brauchen keine Geräte oder anderes Equipment, da alles vom <strong>Personaltrainer</strong> gestellt wird.

						Jeder Kunde erhält einen individuellen Trainingsplan, um somit optimal mit uns zu trainieren.

						In der Regel werden die Klienten ein- oder zweimal die Woche besucht, um ein regelmäßiges Training gewährleisten zu können.

						Desweiteren bieten wir Muskelaufbau, Beseitigung von Rückenproblemen, Hautstraffung, Schwangerschaftsrückbildung, Herzkreislauftraining oder Lauftraining an.<br/><br/>



						Wenn Sie genau das erfahren wollen, dann treten Sie mit mir in Kontakt﻿.<br/><br/><span style="font-size:17px; font-weight:bold;">Ich freue mich auf Sie!</span><br/></p>
						<p>
							<img src="images/template/docs/about/signatur.png"/>
						</p>
			        </div>
			        <div>
					<h2 class="title">Preise <img src="images/template/docs/about/arrow.png"/></h2>
					<p>
						<img src="images/template/docs/about/preise.png"/><br/><br/>
					</p>
					<p>Wenn Sie Fragen zu den Preisen haben oder mich einfach kennen lernen möchten, können Sie sich gerne mit mir in Verbindung setzen, und wir sprechen ausgiebig über den Sachverhalt.

						Der Preis setzt sich immer aus dem resultierenden Aufwand und somit aus den anfallenden Kosten zusammen.

						Wenn ein großer Zeitaufwand benötigt wird und zusätzliche Kosten, wie z.B. Eintrittsgelder anfallen, resultiert ein anderer Preis.<br/><br/>
						<span class="about_head">Einzelkarte</span><br/><br/>

						Wenn Sie unregelmäßig trainieren wollen oder einfach nur einmal eine Stunde professionell sportlich angeleitet werden möchten, gibt es die Möglichkeit, eine Einheit (60/90 Min.) Personal Fitnesstraining zu buchen.<br/>

						Auch als Probestunde kann man, einfach mal zum Austesten, eine Stunde erwerben, um anschließend ein Fazit zu ziehen.<br/><br/>
						<span class="about_head">10er Karten</span><br/><br/>

						Es gibt die Möglichkeit, 10 Stunden auf einmal zu buchen.

						Dies ist für Klienten zu empfehlen, die sich jetzt schon sicher sind, einen längeren Zeitraum mit mir zusammen trainieren zu wollen.<br/><br/>
						<span class="about_head">20er Karte</span><br/><br/>

						Wenn Sie und ich uns sicher sind, dass wir über einen längeren Zeitraum zusammen trainieren werden, gibt es die Möglichkeit, eine 20 Stunden auf einmal zu buchen.

						Dadurch sparen Sie Kosten, und wir schaffen eine konstante Trainingslinie.

						Ich stehe dann mit Rat, Tat und Motivation für Sie zur Verfügung und achte auf das richtige Streben nach den vereinbarten Zielen.
						Dadurch wird die 21. Stunde kostenlos absolviert.
						</p>
					</div>
					<div>
					<h2 class="title">Erfahrungsberichte <img src="images/template/docs/about/arrow.png"/></h2>
					<p>
						<img src="images/template/docs/about/review.png"/><br/><br/>
					</p>
					<p><span class="about_head">Inhaber einer Lackiererei aus Hamburg sagt:</span><br/>

						﻿Mit Jari kann man beim Training richtig Spaß haben.

						Er ist ein bodenständiger junger Mann, der Spaß versteht und auch mal einstecken kann.

						Wir beide kommen sehr gut miteinander klar, und ich freue mich, ihn zu kennen.</p>

						<p>
						<span class="about_head">Leitende Angestellte aus Seevetal sagt:</span><br/>

						Jari ist ein wirklich guter Trainer, durch ihn habe ich Personal Fitnesstraining erst richtig kennen gelernt und möchte es auf gar keinen Fall mehr missen.

						Mit Jari ist wirklich jede Trainingseinheit so effizient, wie es nur geht.

						Ohne ihn wäre es mir gar nicht möglich, so effektiv, gezielt und abwechlslungsreich zu trainieren.

						Die SMS mein Training auch mal alleine durchzuführen, helfen ungemein bei der Ausführung und treiben mich richtig an.

						Jetzt schaffe ich es endlich mal, konstant zu trainieren, und die Erfolge sind deutlich sichtbar.</p> 

						<p>
						<span class="about_head">Hausfrau und Mutter sagt:</span><br/>

						Es ist wirklich motivierend, von Jari angeleitet zu werden.

						Alleine ist es immer so schwer, seinen "inneren Schweinehund" zu übendwinden und sich zum Sport aufzuraffen.

						Aber jetzt mit Jari kann ich gar nicht mehr anders und schaffe es jetzt endlich mal zu einem regelmäßigen Training, was auch tatsächlich super Erfolge zeigt.

						Die zusätzlichen Motivations-SMS an Tagen, wo wir nicht trainieren, sind super und treiben mich zusätzlich an.

						Ich fühle mich immer besser!!!</p>

						<p>
						<span class="about_head">Motivationscoach sagt:</span><br/>

						Selbst als Motivationscoach ist es nicht immer leicht, an seine Grenzen zu gehen.

						Da ich keine Ahnung von Muskeln und Trainingsplänen hatte und ein Mensch bin, der sehr gerne schnell und unkompliziert an seine Ziele kommt, brauchte ich eine professionelle Begleitung.

						Über eine Empfehlung kam ich an Jari und habe ihn als netten, selbstbewussten jungen Mann kennen gelernt.</p>
					</div>

				</div>
			</div>
			<div class="big-screen" style="position:absolute; z-index:1000; top:-168px; right:0;">
				<img src="images/template/docs/about/jari.png"/>
			</div>
		</div>
	</div>	
</section>