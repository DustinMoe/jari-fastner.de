<?php /* --------------------------------------------------------------------

  @author	Dustin Möller <http://minded-design.de/>
			<dustin@minded-design.de>

  Copyright (C) 2013 Dustin Möller. All rights reserved.

  --------------------------------------------------------------------- */ ?>
<section id="service" data-type="background" data-speed="10" class="pages">
	<div class="container">
		<div class="row">
			<div class="span12" style="text-align: center;">
				<br/><br/><br/><br/>
				<img src="images/template/docs/service/headline.png"/>
			</div>
			<div class="span12" style="height:50px;"></div>
			<div class="span3 offset1 service-content heart" style="text-align: center;">
				<img src="images/template/docs/service/heart.png"/><br/><br/>
				<span class="content_text_headline">Gesundheitstraining</span><br/><span class="content_text_subheadline">Herz-kreislauf</span><br/>
				<hr id="service_hr"><br/>
				<div class="service_text">
				Sie haben Probleme mit dem Herzkreislaufsystem oder haben Veranlagungen für Stoffwechselkrankheiten wie z.B. Bluthochdruck, Diabetes, Asthma u.s.w.
				<div class="hide-me">
				Sie wollen, bzw. Ihr Arzt hat Ihnen empfohlen, sich sportlich zu betätigen, dann kann ich Ihnen das individuellste, effizienteste und gesündeste Sportprogramm anbieten, welches auf Ihren Körper zugeschnitten ist.</div></div>
			</div>
			<div class="span8"></div>
			<div class="span3 offset9 service-content bodyforming pull-right" style="text-align: center;">
				<img src="images/template/docs/service/bodyforming.png"/><br/><br/>
				<span class="content_text_headline">Bodyforming</span><br/><span class="content_text_subheadline">Körperstraffung</span><br/>
				<hr id="service_hr">
				<br/>
				<div class="service_text">
				Wenn Sie straffere Haut und einen athletischen Körper anstreben, bin ich gerne mit der richtigen Trainingsmethode und Übungsauswahl für Sie da.
				<div class="hide-me">
				Wir werden zusammen trainieren, und bei jeder Bewegung wird von mir akribisch auf die perfekte Bewegungsausführung geachtet und bei Fehlern sofort korrigiert.

				Sie haben eine höhere Effizienz und somit schnellere Erfolge.</div></div>
			</div>
			<div class="span3 offset1 service-content reduce" style="text-align: center;">
				<img src="images/template/docs/service/reduce-weight.png"/><br/><br/>
				<span class="content_text_headline">Gewichtsreduktion</span><br/><span class="content_text_subheadline">Abnehmen</span><br/>
				<hr id="service_hr">
				<br/>
				<div class="service_text">
				Nach dem Motto "immer versucht, aber nie geschafft" sind Sie auf der Suche nach jemandem, etwas oder einer Methode, endlich, wirklich mal dauerhaft Gewicht zu verlieren und nicht nur noch von Versuch zu Versuch entmutigter zu werden.
				<div class="hide-me">
				Sie möchten abnehmen und somit zu einem neuen Leben mit mehr Lebensqualität und Freude gelangen.

				Dann rufen Sie mich an, und wir können uns gern kostenlos zu einem Beratungsgespräch treffen.</div></div>
			</div>
			<div class="span8"></div>
			<div class="ID-Image"></div> 

		</div>
	</div>
</section>