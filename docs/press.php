<?php /* --------------------------------------------------------------------

  @author	Dustin Möller <http://minded-design.de/>
			<dustin@minded-design.de>

  Copyright (C) 2013 Dustin Möller. All rights reserved.

  --------------------------------------------------------------------- */ ?>
<section id="press">
	<div class="container">
		<div class="row">
			<div class="span12" style="height:150px;"></div>
			<div style="text-align:center; z-index:1010; background:#fff; -webkit-box-shadow: 1px 1px 11px #cbd2c6;
	-moz-box-shadow: 1px 1px 11px #cbd2c6;
	box-shadow: 1px 1px 11px #cbd2c6;" class="span12">
				<br/><br/>
				<div style="text-align:center; ">
					<img src="images/template/docs/pr/headline.png" alt="Presse">
				</div>
				<br/><br/><br/><br/>
				<div id="rg-gallery2" class="rg-gallery2">
					<center>
						<div class="rg-thumbs2" style="max-width:70%;">
							<!-- Elastislide Carousel Thumbnail Viewer -->
							<div class="es-carousel-wrapper2">
								<div class="es-nav2">
									<span class="es-nav-prev2">Previous</span>
									<span class="es-nav-next2">Next</span>
								</div>
								<div class="es-carousel2">
									<ul>
										<li><a href="#"><img src="" data-large="images/template/docs/pr/xello.jpg" alt="image01" data-description="Jari im Xello Magazin" /></a></li>
										<li><a href="#"><img src="" data-large="images/template/docs/pr/rat_tat.jpg" alt="image02" data-description="Jari im Rat & Tat" /></a></li>
										<li><a href="#"><img src="" data-large="images/template/docs/pr/m_u_n_1.png" alt="image03" data-description="Rücken stärken" /></a></li>
										<li><a href="#"><img src="" data-large="images/template/docs/pr/m_u_n_2.png" alt="image04" data-description="Rücken stärken 2" /></a></li>
										<li><a href="#"><img src="" data-large="images/template/docs/pr/MuN_0114_Jari.jpg" alt="image05" data-description="Mensch und Natur" /></a></li>
										<li><a href="#"><img src="" data-large="images/template/docs/pr/MuN_0114_Jari2.jpg" alt="image06" data-description="Mensch und Natur" /></a></li>
										<li><a href="#"><img src="" data-large="images/template/docs/pr/MuN_0114_Jari3.jpg" alt="image07" data-description="Mensch und Natur" /></a></li>
										<li><a href="#"><img src="" data-large="images/template/docs/pr/bericht_alex.jpg" alt="image08" data-description="Jari und Alexander Barwich" /></a></li>
										<li><a href="#"><img src="" data-large="images/template/docs/pr/jari_u_peter.jpg" alt="image09" data-description="Jari mit Peter Pubanz" /></a></li>
									</ul>
								</div>
							</div>
							<!-- End Elastislide Carousel Thumbnail Viewer -->
						</div><!-- rg-thumbs -->
					</center>
				</div><!-- rg-gallery -->
			</div>
			<div class="span12" style="height:150px;"></div>
		</div>
	</div>
</section>