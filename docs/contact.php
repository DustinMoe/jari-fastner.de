<?php /* --------------------------------------------------------------------

  @author	Dustin Möller <http://minded-design.de/>
			<dustin@minded-design.de>

  Copyright (C) 2013 Dustin Möller. All rights reserved.

  --------------------------------------------------------------------- */ ?>
<section id="contact" data-type="background" data-speed="10" class="pages">
	<div class="container">
		<div class="row">
			<div class="span3 offset1" id="contact_form"><center><img  src="images/template/docs/contact/headline.png" /></center><br/><br/>
				<form action="" id="contact-form" class="form-horizontal">
					<fieldset>
						<p>
							Sie Haben Interesse an meinem Angebot bekommen oder wollen ein <strong>Probetraining</strong> ausprobieren? Dann schreiben Sie mir eine E-Mail. Ich <strong>freue</strong> mich etwas von Ihnen zu hören.<br/>
							<br/>
						</p>
						<div class="control-group">
							<!--[if IE]>
							<label class="control-label" for="name">Name:<span class="help-required">*</span></label>
							<div class="controls" class="contact_ie">
							<![endif]-->
								<input type="text" class="input-large" name="name" id="name" placeholder="Name">
								<!--[if IE]>
								</div>
								<![endif]-->
						</div>
						<div class="control-group">
							<!--[if IE]>
							<label class="control-label" for="email">Email:<span class="help-required">*</span></label>
							<div class="controls" class="contact_ie">
							<![endif]-->
								<input type="text" class="input-large" name="email" id="email" placeholder="Email">
								<!--[if IE]>
								</div>
								<![endif]-->
						</div>
						<div class="control-group">
							<!--[if IE]>
							<label class="control-label" for="tel">Telefon:</label>
							<div class="controls"class="contact_ie">
							<![endif]-->
								<input type="number" class="input-large" name="tel" id="tel" placeholder="Telefon">
								<!--[if IE]>
								</div>
								<![endif]-->
						</div>
						<div class="control-group">
							<!--[if IE]>
							<label class="control-label" for="subject">Betreff:<span class="help-required">*</span></label>
							<div class="controls"class="contact_ie">
							<![endif]-->
								<input type="text" class="input-large" name="subject" id="subject" placeholder="Betreff">
								<!--[if IE]>
								</div>
								<![endif]-->
						</div>
						<div class="control-group">
							<!--[if IE]>
							<label class="control-label" for="message">Nachricht:<span class="help-required">*</span></label>
							<div class="controls"class="contact_ie">
							<![endif]-->
								<textarea class="input-large" name="message" id="message" rows="3" placeholder="Nachricht"></textarea>
								<!--[if IE]>
								</div>
								<![endif]-->
						</div>
						<div class="form-actions">
							<center><button name="submit" id="submit" type="submit" class="btn btn-primary">Senden</button></center>
						</div>
					</fieldset>
				</form>
				<div id="success" style="display:none;">Ihre Nachricht wurde verschickt!</div>
			</div>
			<div class="span3 offset1" id="contact_owner" itemscope itemtype="http://schema.org/Organization">
			<strong style="color:#1BB2F5; font-size:24px;"><a href="https://plus.google.com/106662545354100635507?rel=author">Jari Fastner</a></strong><br/><br/>
			<span itemscope itemtype="http://schema.org/PostalAddress">
			<span itemprop="name">Jari Fastner | Personal Fitnesstraining</span>
			<span itemprop="streetAddress">Thomasdamm 11</span><br/>
			<span itemprop="postalCode">21244</span> <span itemprop="addressLocality">Buchholz</span></span><br/>
			<span itemprop="telephone">Tel. 0160/4055789</span><br/>
			<a href="mailto:info@jari-fastner.de" title="E-Mail" itemprop="email">info@jari-fastner.de</span><br/>
			<a title="Personal Fitnesstraining" href="http://www.jari-fastner.de" itemprop="url">www.jari-fastner.de</a><br/><br/>
			<strong>Umsetzung: </strong><a target="_blank" href="http://www.minded-design.de" alt="Minded Design Neu Wulmstorf"><img alt="Minded Design Logo"  src="images/template/docs/contact/minded-design.png" /></a>
			<br/><br/>
			<a href="#myModal" role="button" class="btn" data-toggle="modal">Impressum</a>
			</div>
			<div class="span3" id="contact_facebook"><a href="https://www.facebook.com/JariFastnerPersonalFitnesstraining"><img  src="images/template/docs/contact/facebook.png" /></a></div>
		</div>
		<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h3 id="myModalLabel">Impressum</h3>
				</div>
				<div class="modal-body">
				<p>
				Anbieter:<br/>
				Jari Fastner<br/>
				Thomasdamm 11<br/>
				21244 Buchholz<br/>
				Tel. 0160/4055789<br/>
				info@jari-fastner.de<br/><br/>
				Umsatzsteuer-Identifikationsnummer: DE 15/112/07741<br/><br/>

				Internet:http://www.jari-fastner.de<br/><br/>

				Design und Umsetzung:<br/>
				Minded Design<br/><br/>

				Technischer Ansprechpartner:<br/>
				Minded Design<br/>
				Inh. Dustin Möller<br/>
				Masurenweg 4<br/>
				21629 Neu Wulmstorf<br/>
				0176/32533420<br/>
				info@minded-design.de<br/><br/>
<br/>
				Urheberrecht:<br/><br/>

				Die Texte, Grafiken und Fotos auf diesen Seiten unterliegen dem deutschen Urheberrecht. Jede Art der kommerziellen Verwertung, insbesondere der Bearbeitung, Verbreitung und Vervielfältigung ist nur mit der schriftlichen Zustimmung des jeweiligen Urhebers gestattet. Beiträge Dritter sind als solche gekennzeichnet.<br/><br/>

				Rechtlicher Hinweis:<br/><br/>

				Rechtlicher Hinweis Durch das Urteil vom 12. Mai 1998 – 312 O 85/98 – »Haftung für Links« hat das Landgericht Hamburg entschieden, dass man durch die Anbringung eines Links die Inhalte der gelinkten Seite ggf. mit zu verantworten hat. Dies kann nur dadurch verhindert werden, dass man sich ausdrücklich von diesen Inhalten distanziert. Wir haben auf verschiedenen Seiten dieser Website Links zu anderen Sites gelegt. Diese Links sind als “externe links” gekennzeichnet. Wir weisen ausdrücklich darauf hin, dass wir keinerlei Einfluss auf die Gestaltung und die Inhalte dieser gelinkten Seiten haben. Deshalb distanzieren wir uns von den Inhalten dieser Seiten und machen uns ihre Inhalte nicht zu eigen. Wir sind dankbar für jeden Hinweis auf inhaltlich bedenkliche Seiten, die mittels Link an unsere Seiten angebunden sind.
				<p>
				</div>
				<div class="modal-footer">
				<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Schließen</button>
			</div>
		</div>
	</div>
</section>