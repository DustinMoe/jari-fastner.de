<?php /* --------------------------------------------------------------------

  @author	Dustin Möller <http://minded-design.de/>
			<dustin@minded-design.de>

  Copyright (C) 2013 Dustin Möller. All rights reserved.

  --------------------------------------------------------------------- */ ?>
<section id="photo" data-type="background" data-speed="10" class="pages">
	<div class="container">
		<div class="row">
			<div class="span12" style="height:150px;"></div>
			<div style="text-align:center; z-index:1010; background:#fff; -webkit-box-shadow: 1px 1px 19px #cbd2c6;
	-moz-box-shadow: 1px 1px 19px #cbd2c6;
	box-shadow: 1px 1px 19px #cbd2c6;" class="span12">
				<div id="blubb">
					<br/><br/>
					<div style="text-align:center; ">
						<img src="images/template/docs/photo/headline.png" />
					</div>
				<br/><br/><br/><br/>
					<div id="rg-gallery" class="rg-gallery2">
						<center>
							<div class="rg-thumbs" style="max-width:70%;">
								<!-- Elastislide Carousel Thumbnail Viewer -->
								<div class="es-carousel-wrapper">
									<div class="es-nav">
										<span class="es-nav-prev">Previous</span>
										<span class="es-nav-next">Next</span>
									</div>
									<div class="es-carousel">
										<ul>
											<li><a href="#"><img src="images/template/docs/photo/thumbs/jf_gallery_bild01_thumb.jpg" data-large="images/template/docs/photo/fotos/jf_gallery_bild01.jpg" alt="image01" data-description="" /></a></li>
											<li><a href="#"><img src="images/template/docs/photo/thumbs/jf_gallery_bild02_thumb.jpg" data-large="images/template/docs/photo/fotos/jf_gallery_bild02.jpg" alt="image02" data-description="" /></a></li>
											<li><a href="#"><img src="images/template/docs/photo/thumbs/jf_gallery_bild03_thumb.jpg" data-large="images/template/docs/photo/fotos/jf_gallery_bild03.jpg" alt="image03" data-description="" /></a></li>
											<li><a href="#"><img src="images/template/docs/photo/thumbs/jf_gallery_bild04_thumb.jpg" data-large="images/template/docs/photo/fotos/jf_gallery_bild04.jpg" alt="image04" data-description="" /></a></li>
											<li><a href="#"><img src="images/template/docs/photo/thumbs/jf_gallery_bild05_thumb.jpg" data-large="images/template/docs/photo/fotos/jf_gallery_bild05.jpg" alt="image05" data-description="" /></a></li>
											<li><a href="#"><img src="images/template/docs/photo/thumbs/jf_gallery_bild06_thumb.jpg" data-large="images/template/docs/photo/fotos/jf_gallery_bild06.jpg" alt="image06" data-description="" /></a></li>
											<li><a href="#"><img src="images/template/docs/photo/thumbs/jf_gallery_bild07_thumb.jpg" data-large="images/template/docs/photo/fotos/jf_gallery_bild07.jpg" alt="image07" data-description="" /></a></li>
											<li><a href="#"><img src="images/template/docs/photo/thumbs/jf_gallery_bild08_thumb.jpg" data-large="images/template/docs/photo/fotos/jf_gallery_bild08.jpg" alt="image08" data-description="" /></a></li>
											<li><a href="#"><img src="images/template/docs/photo/thumbs/jf_gallery_bild09_thumb.jpg" data-large="images/template/docs/photo/fotos/jf_gallery_bild09.jpg" alt="image09" data-description="" /></a></li>
											<li><a href="#"><img src="images/template/docs/photo/thumbs/jf_gallery_bild10_thumb.jpg" data-large="images/template/docs/photo/fotos/jf_gallery_bild10.jpg" alt="image10" data-description="" /></a></li>
										</ul>
									</div>
								</div>
								<!-- End Elastislide Carousel Thumbnail Viewer -->
							</div><!-- rg-thumbs -->
						</center>
					</div><!-- rg-gallery -->
				</div>
			</div>
			<div class="span12" style="height:150px;"></div>
		</div>
	</div>
</section>

