<?php /* --------------------------------------------------------------------

  @author	Dustin Möller <http://minded-design.de/>
			<dustin@minded-design.de>

  Copyright (C) 2013 Dustin Möller. All rights reserved.

  --------------------------------------------------------------------- */ ?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta content="Jari Fastner | Personal Fitnesstraining" name="title">
		<meta content="Jari Fastner | Personal Fitnesstraining" name="DC.Title">
		<meta content="Jari Fastner | Personal Fitnesstraining" http-equiv="title">
		<meta name="author" content="Jari Fastner">
		<meta name="page-topic" content="Dienstleistung">
		<meta name="robots" content="index,follow">
		<meta name="keywords" content="personal fitnesstraining,personal trainer,personaltrainer,buxtehude,buchholz,umgebung,hamburg,harburg,stade,training,fitnesstrainer,jari fastner,abnehmen,gewichtsreduktion,straffung,muskeln aufbauen,ernährungsberatung,lifecoach,boxen,lauftrainer,rückenprobleme,hittfeld,seevetal,rosengarten,tötensen,hanstedt,schneverdingen,nordheide,tostedt,hollenstedt,jesteburg,bendestorf">
		<meta http-equiv="keywords" content="Personal Fitnesstraining,Buxtehude,Buchholz in der Nordheide,Stade,Training,Fitnesstrainer,Jari Fastner">
		<meta property="og:locale" content="de_DE">
		<meta property="og:type" content="website">
		<meta property="og:title" content="Jari Fastner | Personal Fitnesstraining in Buchholz i.d.N. und Umgebung">
		<meta property="og:description" content="Personaltrainer Buchholz. Verliere Gewicht - Gewinne Leben. Gelangen Sie zu neuer Freude, Energie und Lebensqualität! Abnehmen - Gewichtsreduktion - Gesundheitstraining.">
		<meta property="og:url" content="http://www.jari-fastner.de/">
		<meta property="og:site_name" content="Jari Fastner | Personal Fitnesstraining">
		<meta property="article:publisher" content="https://plus.google.com/u/0/106662545354100635507/">
		<meta property="og:image" content="http://www.jari-fastner.de/logo.gif">
		<meta property="og:locale:alternate" content="de_DE">
		<link rel="author" href="https://plus.google.com/u/0/106662545354100635507/">
		<link rel="publisher" href="https://plus.google.com/u/0/106662545354100635507/">
		<meta name="description" content="Jari Fastner - Personalfitnesstraining. Verliere Gewicht - Gewinne Leben. Gelangen Sie zu neuer Freude, Energie und Lebensqualität! Abnehmen - Gewichtsreduktion - Gesundheitstraining. Buchholz, Hamburg und Umgebung.">
		<title id="title">Jari Fastner | Personal Fitnesstraining*Trainer für Buchholz i.d.N.</title>
		<link rel="shortcut icon" href="images/template/assets/favicon.ico">
		<link href='http://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>	
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,600,400' rel='stylesheet' type='text/css'>
		<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
		<script src="./js/jquery-ui-1.8.20.custom.min.js"></script>
		<script src="js/bootstrap.js"></script>
		<script src="js/modal.js"></script>
		<script src="js/jquery.liquid-slider.js"></script>
		<script src="js/jquery.validate.min.js"></script>
		<script src="js/jquery.tmpl.min.js"></script>
		<script src="js/jquery.easing.1.3.js"></script>
		<script src="js/jquery.elastislide.js"></script>
		<script src="js/gallery.js"></script>
		<link rel="stylesheet" href="style/liquid-slider.css"/>
		<link rel="stylesheet" href="style/bootstrap.css" />
		<link rel="stylesheet" href="style/contact.css" />
		<link rel="stylesheet" href="style/style.css"/>
		<link rel="stylesheet" href="style/elastislide.css"/>
		<link rel="stylesheet" href="style/modal.css"/>
		<script src="js/index.js"></script>
		<script id="img-wrapper-tmpl" type="text/x-jquery-tmpl">	
			<div class="rg-image-wrapper">
				{{if itemsCount > 1}}
					<div class="rg-image-nav">
						<a href="#" class="rg-image-nav-prev">Previous Image</a>
						<a href="#" class="rg-image-nav-next">Next Image</a>
					</div>
				{{/if}}
				<div class="rg-image"></div>
				<div class="rg-loading"></div>
				<div class="rg-caption-wrapper">
					<div class="rg-caption" style="display:none;">
						<p></p>
					</div>
				</div>
			</div>
			<hr></br>
		</script>
		<script id="img-wrapper-tmpl2" type="text/x-jquery-tmpl">	
			<div class="rg-image-wrapper2">
				{{if itemsCount > 1}}
					<div class="rg-image-nav2">
						<a href="#" class="rg-image-nav-prev2">Previous Image</a>
						<a href="#" class="rg-image-nav-next2">Next Image</a>
					</div>
				{{/if}}
				<div class="rg-caption2" style="display:none;">
					<p></p>
				</div>
				<div class="rg-image2"></div>
				<div class="rg-loading2"></div>
				<div class="rg-caption-wrapper2">
					
				</div>
			</div>
			<hr></br>
		</script>
	</head>
	<body data-spy="scroll" data-target=".navbar">
		<header class="navbar navbar-inverse navbar-fixed-top">
			<h1 class="opt">Personal Trainer Buchholz i.d.N. Jari Fastner - Spezialist in Niedersachsen für Gewichtsreduktion | Abnehmen - Personaltraining & Ernährungsberatung | Straffung | Muskeln aufbauen | Rückenprobleme</h1>
			<div class="navbar-inner">
				<section class="container">
					<div class="span12 clear_top visible-desktop"></div>
					<a class="btn btn-navbar collapsed" data-target=".nav-collapse" data-toggle="collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</a>
					<div class="container">
						<a class="brand pull-left" href="#">
							<img src="images/template/logo.png" />
						</a>
						<div class="nav-collapse collapse md_links">
							<ul class="nav">
								<li><a id="start" href="#header">STARTSEITE</a></li>
								<li><a href="#about" id="about_me">ÜBER MICH</a></li>
								<li><a href="#service">LEISTUNG</a></li>
								<li><a href="#about" id="about_preise" >PREISE</a></li>
								<li><a href="#photo">FOTOS</a></li>
								<li><a href="#press">PRESSE</a></li>
								<li><a href="#about" id="about_review">ERFAHRUNGSBERICHTE</a></li>
								<li><a href="#contact">KONTAKT</a></li>
							</ul>
						</div>
						<a id="logo_right" class="pull-right big-screen" href="#">
							<img src="images/template/logo_right.png" />
						</a>
					</div>
				</section>
			</div>
		</header>
			<!-- CONTENT -->
				<section>
						<?php include ('docs/header.php'); ?>
						<?php include ('docs/service.php'); ?>
						<?php include ('docs/about.php'); ?>
						<?php include ('docs/photo.php'); ?>
						<?php include ('docs/press.php'); ?>
						<?php include ('docs/contact.php'); ?>
				</section>
			<!-- CONTENT ENDE -->
			<footer>
				<script type="text/javascript">
				  var _gaq = _gaq || [];
				  _gaq.push(['_setAccount', 'UA-36961233-1']);
				  _gaq.push(['_trackPageview']);

				  (function() {
				    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
				  })();
				</script>
			</footer>
	</body>
</html>